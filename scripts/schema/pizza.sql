DROP TABLE IF EXISTS pizza;

CREATE TABLE pizza (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    style_id INT NOT NULL,
    CONSTRAINT fk_style FOREIGN KEY (style_id) REFERENCES style(id)
);
