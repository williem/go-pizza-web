DROP TABLE IF EXISTS rel_pizza_size;

CREATE TABLE rel_pizza_size (
    pizza_id INT NOT NULL,
    size_id INT NOT NULL,
    PRIMARY KEY (pizza_id, size_id),
    CONSTRAINT fk_pizza FOREIGN KEY (pizza_id) REFERENCES pizza(id),
    CONSTRAINT fk_size FOREIGN KEY (size_id) REFERENCES size(id)
)
