package main

import (
	"log"
	"net/http"
	"os"
	"path"

	"pizzeria/pkg/routes"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func main() {
	// init DB
	// db := database.DB{}
	// db.InitDB()

	// env := services.Env{}
	// env.InitServices(db.DB)

	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Mount("/", routes.HomeViewRoutes())

	r.Mount("/v1/style", routes.StyleRoutes())
	r.Mount("/style", routes.StyleViewRoutes())

	r.Mount("/v1/size", routes.SizeRoutes())
	r.Mount("/size", routes.SizeViewRoutes())

	r.Mount("/v1/pizza", routes.PizzaRoutes())
	r.Mount("/pizza", routes.PizzaViewRoutes())

	workDir, err := os.Getwd()
	if err != nil {
		log.Println("Error in processing working directory")
	}

	iconPath := path.Join(workDir, "public/asset/img/icon.png")

	r.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, iconPath)
	})

	// stupidly handle js path
	jsPath := path.Join(workDir, "public/asset/js")

	r.Handle("/js/*", http.StripPrefix("/js", http.FileServer(http.Dir(jsPath))))

	http.ListenAndServe(":3000", r)
}
