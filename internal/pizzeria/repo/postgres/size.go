package postgres

import (
	"context"
	"pizzeria/pkg/models"

	"github.com/jackc/pgx/v4/pgxpool"
)

type sizeRepo struct {
	db *pgxpool.Pool
}

type ISize interface {
	GetAll() ([]*models.Size, error)
	GetById(id int) (*models.Size, error)
	Create(style models.Size) error
	Update(id int, style models.Size) error
	Delete(id int) error
}

func InitSizeRepo(db *pgxpool.Pool) *sizeRepo {
	return &sizeRepo{db: db}
}

func (s *sizeRepo) GetAll() ([]*models.Size, error) {
	var result []*models.Size
	rows, err := s.db.Query(context.Background(), "SELECT id, name FROM size")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var size models.Size
		if err := rows.Scan(&size.ID, &size.Name); err != nil {
			return nil, err
		}

		result = append(result, &size)
	}

	return result, nil
}

func (s *sizeRepo) GetById(id int) (*models.Size, error) {
	var result models.Size
	row := s.db.QueryRow(context.Background(), "SELECT id, name FROM size WHERE id = $1", id)
	if err := row.Scan(&result.ID, &result.Name); err != nil {
		return nil, err
	}
	return &result, nil
}

func (s *sizeRepo) Create(size models.Size) error {
	if _, err := s.db.Exec(context.Background(), "INSERT INTO size (name) VALUES ($1)", size.Name); err != nil {
		return err
	}
	return nil
}

func (s *sizeRepo) Update(id int, size models.Size) error {
	if _, err := s.db.Exec(context.Background(), "UPDATE size SET name = $1 WHERE id = $2", size.Name, id); err != nil {
		return err
	}
	return nil
}

func (s *sizeRepo) Delete(id int) error {
	if _, err := s.db.Exec(context.Background(), "DELETE FROM size WHERE id = $1", id); err != nil {
		return err
	}
	return nil
}
