package postgres

import (
	"context"
	"pizzeria/pkg/models"

	"github.com/jackc/pgx/v4/pgxpool"
)

type pizzaRepo struct {
	db *pgxpool.Pool
}

type IPizza interface {
	GetAll() ([]*models.Pizza, error)
	GetById(id int) (*models.Pizza, error)
	Create(pizza models.Pizza) error
	Update(id int, pizza models.Pizza) error
	Delete(id int) error
}

func InitPizzaRepo(db *pgxpool.Pool) *pizzaRepo {
	return &pizzaRepo{db: db}
}

func (p *pizzaRepo) GetAll() ([]*models.Pizza, error) {
	var result []*models.Pizza
	rows, err := p.db.Query(context.Background(), "SELECT id, name, style_id FROM pizza")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var pizza models.Pizza
		if err := rows.Scan(&pizza.ID, &pizza.Name, &pizza.StyleID); err != nil {
			return nil, err
		}

		// get Style and assign to Pizza (ORM style) (probably not a good idea?)
		// notice: call the styleRepo in the same package and pass the same db from pizzaRepo
		style, err := (&styleRepo{db: p.db}).GetById(pizza.StyleID)
		if err != nil {
			return nil, err
		}
		pizza.Style = style

		result = append(result, &pizza)
	}

	return result, nil
}

func (p *pizzaRepo) GetById(id int) (*models.Pizza, error) {
	var result models.Pizza
	row := p.db.QueryRow(context.Background(), "SELECT id, name, style_id FROM pizza WHERE id = $1", id)
	if err := row.Scan(&result.ID, &result.Name, &result.StyleID); err != nil {
		return nil, err
	}
	return &result, nil
}

func (p *pizzaRepo) Create(pizza models.Pizza) error {
	query := "INSERT INTO pizza (name, style_id) VALUES ($1, $2)"
	if _, err := p.db.Exec(context.Background(), query, pizza.Name, pizza.StyleID); err != nil {
		return err
	}
	return nil
}

func (p *pizzaRepo) Update(id int, pizza models.Pizza) error {
	query := "UPDATE pizza SET name = $1, style_id = $2 WHERE id = $3"
	if _, err := p.db.Exec(context.Background(), query, pizza.Name, pizza.StyleID, id); err != nil {
		return err
	}
	return nil
}

func (p *pizzaRepo) Delete(id int) error {
	if _, err := p.db.Exec(context.Background(), "DELETE FROM pizza WHERE id = $1", id); err != nil {
		return err
	}
	return nil
}
