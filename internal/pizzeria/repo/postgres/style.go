package postgres

import (
	"context"
	"pizzeria/pkg/models"

	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4/pgxpool"
)

type styleRepo struct {
	db *pgxpool.Pool
}

func InitStyleRepo(db *pgxpool.Pool) *styleRepo {
	return &styleRepo{db: db}
}

func (s *styleRepo) GetAll() ([]*models.Style, error) {
	var result []*models.Style
	query, _, _ := sq.Select("id", "name").From("style").ToSql() // use squirrel to do query building
	rows, err := s.db.Query(context.Background(), query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var style models.Style
		if err := rows.Scan(&style.ID, &style.Name); err != nil {
			return nil, err
		}

		result = append(result, &style)
	}

	// if err := rows.Err(); err != nil {
	// 	return nil, err
	// }
	return result, nil
}

func (s *styleRepo) GetById(id int) (*models.Style, error) {
	var result models.Style
	row := s.db.QueryRow(context.Background(), "SELECT id, name FROM style WHERE id = $1", id)
	if err := row.Scan(&result.ID, &result.Name); err != nil {
		return nil, err
	}
	return &result, nil
}

func (s *styleRepo) Create(style models.Style) error {
	_, err := s.db.Exec(context.Background(), "INSERT INTO style (name) VALUES ($1)", style.Name)
	if err != nil {
		return err
	}

	return nil
}

func (s *styleRepo) Update(id int, style models.Style) error {
	_, err := s.db.Exec(context.Background(), "UPDATE style SET name = $1 WHERE id = $2", style.Name, id)
	if err != nil {
		return err
	}
	return nil
}

func (s *styleRepo) Delete(id int) error {
	if _, err := s.db.Exec(context.Background(), "DELETE FROM style WHERE id = $1", id); err != nil {
		return err
	}
	return nil
}
