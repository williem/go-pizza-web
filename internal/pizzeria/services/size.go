package services

import (
	db "pizzeria/internal/database/postgres"
	repositories "pizzeria/internal/pizzeria/repo/postgres"
	"pizzeria/pkg/models"
)

type sizeService struct {
	Sizes repositories.ISize
}

func InitSizeService() *sizeService {
	return &sizeService{
		Sizes: repositories.InitSizeRepo(db.DB),
	}
}

func (s *sizeService) GetAll() ([]*models.Size, error) {
	sizes, err := s.Sizes.GetAll()
	return sizes, err
}

func (s *sizeService) GetById(id int) (*models.Size, error) {
	size, err := s.Sizes.GetById(id)
	return size, err
}

func (s *sizeService) Create(size models.Size) error {
	if err := s.Sizes.Create(size); err != nil {
		return err
	}
	return nil
}

func (s *sizeService) Update(id int, size models.Size) error {
	if err := s.Sizes.Update(id, size); err != nil {
		return err
	}
	return nil
}

func (s *sizeService) Delete(id int) error {
	if err := s.Sizes.Delete(id); err != nil {
		return err
	}
	return nil
}
