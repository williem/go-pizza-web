package services

import (
	db "pizzeria/internal/database/postgres"
	repositories "pizzeria/internal/pizzeria/repo/postgres"
	"pizzeria/pkg/models"
)

type styleService struct {
	Styles models.IStyle
}

func InitStyleService() *styleService {
	// return the styleService struct passed with Style attribute
	// initiated with db connection. The reason we put this here is
	// every single pool/connection must be instantiated per service
	// and not request
	return &styleService{
		Styles: repositories.InitStyleRepo(db.DB),
	}
}

func (s *styleService) GetAll() ([]*models.Style, error) {
	// blindly return the error and the styles if any
	styles, err := s.Styles.GetAll()
	return styles, err
}

func (s *styleService) GetById(id int) (*models.Style, error) {
	// blindly return the error and the style if any
	style, err := s.Styles.GetById(id)
	return style, err
}

func (s *styleService) Create(style models.Style) error {
	if err := s.Styles.Create(style); err != nil {
		return err
	}
	return nil
}

func (s *styleService) Update(id int, style models.Style) error {
	if err := s.Styles.Update(id, style); err != nil {
		return err
	}
	return nil
}

func (s *styleService) Delete(id int) error {
	if err := s.Styles.Delete(id); err != nil {
		return err
	}
	return nil
}
