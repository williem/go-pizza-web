package services

import (
	db "pizzeria/internal/database/postgres"
	repositories "pizzeria/internal/pizzeria/repo/postgres"
	"pizzeria/pkg/models"
)

type pizzaService struct {
	Pizzas repositories.IPizza
}

func InitPizzaService() *pizzaService {
	return &pizzaService{
		Pizzas: repositories.InitPizzaRepo(db.DB),
	}
}

func (p *pizzaService) GetAll() ([]*models.Pizza, error) {
	pizzas, err := p.Pizzas.GetAll()
	return pizzas, err
}

func (p *pizzaService) GetById(id int) (*models.Pizza, error) {
	pizza, err := p.Pizzas.GetById(id)
	return pizza, err
}

func (p *pizzaService) Create(pizza models.Pizza) error {
	if err := p.Pizzas.Create(pizza); err != nil {
		return err
	}
	return nil
}

func (p *pizzaService) Update(id int, pizza models.Pizza) error {
	if err := p.Pizzas.Update(id, pizza); err != nil {
		return err
	}
	return nil
}

func (p *pizzaService) Delete(id int) error {
	if err := p.Pizzas.Delete(id); err != nil {
		return err
	}
	return nil
}
