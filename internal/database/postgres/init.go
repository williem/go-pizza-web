package postgres

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "go_user"
	password = "go_user"
	dbname   = "go_pizzeria"
)

// TODO put me in struct

// type DB struct {
// 	DB *pgxpool.Pool
// }

// func (d *DB) InitDB() {
// 	connString := fmt.Sprintf("host=%s port=%d user=%s "+
// 		"password=%s dbname=%s sslmode=disable",
// 		host, port, user, password, dbname)
// 	conn, err := pgxpool.Connect(context.Background(), connString)
// 	if err != nil {
// 		log.Println("Database Connection Error")
// 		log.Fatalln(err)
// 		os.Exit(1)
// 	}

// 	log.Println("Database Connection Established")
// 	d.DB = conn
// }

var DB *pgxpool.Pool // stick with global var first

func init() {
	connString := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	conn, err := pgxpool.Connect(context.Background(), connString)
	if err != nil {
		log.Println("Database Connection Error")
		log.Fatalln(err)
		os.Exit(1)
	}
	conn.Config().MinConns = 10
	conn.Config().MaxConns = 100
	conn.Config().MaxConnIdleTime = 5 * time.Minute
	conn.Config().MaxConnLifetime = 60 * time.Minute

	log.Println("Database Connection Established")

	DB = conn
}
