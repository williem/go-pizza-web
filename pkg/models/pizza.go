package models

// Pizza model
type Pizza struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	StyleID int    `json:"style_id"`
	Style   *Style `json:",omitempty"`
}
