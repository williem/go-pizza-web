package models

type Style struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type IStyle interface {
	GetAll() ([]*Style, error)
	GetById(id int) (*Style, error)
	Create(style Style) error
	Update(id int, style Style) error
	Delete(id int) error
}
