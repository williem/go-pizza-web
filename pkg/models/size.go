package models

type Size struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
