package routes

import (
	"net/http"
	"pizzeria/pkg/controllers"

	"github.com/go-chi/chi/v5"
)

// StyleRoutes returns http handler function with chi Routes
func StyleRoutes() http.Handler {
	r := chi.NewRouter()
	r.Get("/", controllers.StyleIndex)
	r.Get("/{id}", controllers.StyleDetail)
	r.Post("/create", controllers.StyleCreate)
	r.Post("/update/{id}", controllers.StyleUpdate)
	r.Post("/delete/{id}", controllers.StyleDelete)
	return r
}

func StyleViewRoutes() http.Handler {
	r := chi.NewRouter()
	r.Get("/", controllers.StyleIndexView)
	r.Get("/create", controllers.StyleCreateView)
	r.Post("/create", controllers.StyleCreateView)
	r.Get("/update/{id}", controllers.StyleUpdateView)
	r.Post("/update/{id}", controllers.StyleUpdateView)
	return r
}
