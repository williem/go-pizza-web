package routes

import (
	"net/http"
	"pizzeria/pkg/controllers"

	"github.com/go-chi/chi/v5"
)

func HomeViewRoutes() http.Handler {
	r := chi.NewRouter()
	r.Get("/", controllers.HomeIndex)
	return r
}
