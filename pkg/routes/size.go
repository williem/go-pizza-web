package routes

import (
	"net/http"
	"pizzeria/pkg/controllers"

	"github.com/go-chi/chi/v5"
)

// SizeRoutes returns http handler function with chi Routes
func SizeRoutes() http.Handler {
	r := chi.NewRouter()
	r.Get("/", controllers.SizeIndex)
	r.Get("/{id}", controllers.SizeDetail)
	r.Post("/create", controllers.SizeCreate)
	r.Post("/update/{id}", controllers.SizeUpdate)
	r.Post("/delete/{id}", controllers.SizeDelete)
	return r
}

func SizeViewRoutes() http.Handler {
	r := chi.NewRouter()
	r.Get("/", controllers.SizeIndexView)
	r.Get("/create", controllers.SizeCreateView)
	r.Post("/create", controllers.SizeCreateView)
	r.Get("/update/{id}", controllers.SizeUpdateView)
	r.Post("/update/{id}", controllers.SizeUpdateView)
	return r
}
