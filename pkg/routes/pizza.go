package routes

import (
	"net/http"
	"pizzeria/pkg/controllers"

	"github.com/go-chi/chi/v5"
)

func PizzaRoutes() http.Handler {
	r := chi.NewRouter()
	r.Get("/", controllers.PizzaIndex)
	r.Get("/{id}", controllers.PizzaDetail)
	r.Post("/create", controllers.PizzaCreate)
	r.Post("/update/{id}", controllers.PizzaUpdate)
	r.Post("/delete/{id}", controllers.PizzaDelete)
	return r
}

func PizzaViewRoutes() http.Handler {
	r := chi.NewRouter()
	r.Get("/", controllers.PizzaIndexView)
	r.Get("/create", controllers.PizzaCreateView)
	r.Post("/create", controllers.PizzaCreateView)
	r.Get("/update/{id}", controllers.PizzaUpdateView)
	r.Post("/update/{id}", controllers.PizzaUpdateView)
	return r
}
