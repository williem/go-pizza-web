package controllers

import (
	"encoding/json"
	"html/template"
	"net/http"
	"pizzeria/internal/pizzeria/services"
	"pizzeria/pkg/models"
	"strconv"

	"github.com/go-chi/chi/v5"
)

var styleEnv = services.InitStyleService()

func StyleIndex(w http.ResponseWriter, r *http.Request) {
	styles, err := styleEnv.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonStyle, err := json.Marshal(styles)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.Write(jsonStyle)
}

func StyleDetail(w http.ResponseWriter, r *http.Request) {
	// get id from URL param
	id := chi.URLParam(r, "id")

	// convert to integer
	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// find the record
	style, err := styleEnv.GetById(o_id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonStyle, err := json.Marshal(style)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.Write(jsonStyle)
}

func StyleCreate(w http.ResponseWriter, r *http.Request) {
	// get data from r.Body and decode it into a new Style struct
	var style models.Style
	if err := json.NewDecoder(r.Body).Decode(&style); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// create record
	if err := styleEnv.Create(style); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func StyleUpdate(w http.ResponseWriter, r *http.Request) {
	// get id and validate
	id := chi.URLParam(r, "id")

	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if _, err := styleEnv.GetById(o_id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// parse and update
	var style models.Style
	if err := json.NewDecoder(r.Body).Decode(&style); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := styleEnv.Update(o_id, style); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func StyleDelete(w http.ResponseWriter, r *http.Request) {
	// get id and validate
	id := chi.URLParam(r, "id")

	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if _, err := styleEnv.GetById(o_id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := styleEnv.Delete(o_id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func StyleIndexView(w http.ResponseWriter, r *http.Request) {
	// TODO for list of files, could we just create a function to wrap and return FS?
	// list of files
	var files = []string{
		"pkg/templates/base.html",
		"pkg/templates/style/index.html",
	}
	var templates = template.Must(template.ParseFiles(files...))

	styles, _ := styleEnv.GetAll() // get all style records and pass to template

	if err := templates.ExecuteTemplate(w, "base.html", styles); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func StyleCreateView(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		name := r.FormValue("name")
		style := models.Style{Name: name}
		if err := styleEnv.Create(style); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/style", http.StatusSeeOther)
	}

	// list of files
	var files = []string{
		"pkg/templates/base.html",
		"pkg/templates/form.html",
		"pkg/templates/style/create.html",
	}
	var templates = template.Must(template.ParseFiles(files...))

	if err := templates.ExecuteTemplate(w, "base.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func StyleUpdateView(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	style, err := styleEnv.GetById(o_id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if r.Method == "POST" {
		name := r.FormValue("name")
		style := models.Style{Name: name}
		if err := styleEnv.Update(o_id, style); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/style", http.StatusSeeOther)
	}

	// list of files
	var files = []string{
		"pkg/templates/base.html",
		"pkg/templates/form.html",
		"pkg/templates/style/update.html",
	}
	var templates = template.Must(template.ParseFiles(files...))

	if err := templates.ExecuteTemplate(w, "base.html", style); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
