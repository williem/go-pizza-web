package controllers

import (
	"encoding/json"
	"html/template"
	"net/http"
	"pizzeria/internal/pizzeria/services"
	"pizzeria/pkg/models"
	"strconv"

	"github.com/go-chi/chi/v5"
)

var sizeEnv = services.InitSizeService()

func SizeIndex(w http.ResponseWriter, r *http.Request) {
	sizes, err := sizeEnv.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonSize, err := json.Marshal(sizes)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.Write(jsonSize)
}

func SizeDetail(w http.ResponseWriter, r *http.Request) {
	// get id from URL param
	id := chi.URLParam(r, "id")

	// convert to integer
	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// find the record
	size, err := sizeEnv.GetById(o_id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonSize, err := json.Marshal(size)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.Write(jsonSize)
}

func SizeCreate(w http.ResponseWriter, r *http.Request) {
	// get data from r.Body and decode it into a new Size struct
	var size models.Size
	if err := json.NewDecoder(r.Body).Decode(&size); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// create record
	if err := sizeEnv.Create(size); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func SizeUpdate(w http.ResponseWriter, r *http.Request) {
	// get id and validate
	id := chi.URLParam(r, "id")

	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if _, err := sizeEnv.GetById(o_id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// parse and update
	var size models.Size
	if err := json.NewDecoder(r.Body).Decode(&size); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := sizeEnv.Update(o_id, size); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func SizeDelete(w http.ResponseWriter, r *http.Request) {
	// get id and validate
	id := chi.URLParam(r, "id")

	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if _, err := sizeEnv.GetById(o_id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := sizeEnv.Delete(o_id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func SizeIndexView(w http.ResponseWriter, r *http.Request) {
	// TODO for list of files, could we just create a function to wrap and return FS?
	// list of files
	var files = []string{
		"pkg/templates/base.html",
		"pkg/templates/size/index.html",
	}
	var templates = template.Must(template.ParseFiles(files...))

	sizes, _ := sizeEnv.GetAll() // get all size records and pass to template

	if err := templates.ExecuteTemplate(w, "base.html", sizes); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func SizeCreateView(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		name := r.FormValue("name")
		size := models.Size{Name: name}
		if err := sizeEnv.Create(size); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/size", http.StatusSeeOther)
	}

	// list of files
	var files = []string{
		"pkg/templates/base.html",
		"pkg/templates/form.html",
		"pkg/templates/size/create.html",
	}
	var templates = template.Must(template.ParseFiles(files...))

	if err := templates.ExecuteTemplate(w, "base.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func SizeUpdateView(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	size, err := sizeEnv.GetById(o_id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if r.Method == "POST" {
		name := r.FormValue("name")
		size := models.Size{Name: name}
		if err := sizeEnv.Update(o_id, size); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/size", http.StatusSeeOther)
	}

	// list of files
	var files = []string{
		"pkg/templates/base.html",
		"pkg/templates/form.html",
		"pkg/templates/size/update.html",
	}
	var templates = template.Must(template.ParseFiles(files...))

	if err := templates.ExecuteTemplate(w, "base.html", size); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
