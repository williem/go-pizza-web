package controllers

import (
	"encoding/json"
	"html/template"
	"net/http"
	"pizzeria/internal/pizzeria/services"
	"pizzeria/pkg/models"
	"strconv"

	"github.com/go-chi/chi/v5"
)

var pizzaEnv = services.InitPizzaService()

func PizzaIndex(w http.ResponseWriter, r *http.Request) {
	pizzas, err := pizzaEnv.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonPizza, err := json.Marshal(pizzas)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.Write(jsonPizza)
}

func PizzaDetail(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	pizza, err := pizzaEnv.GetById(o_id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonPizza, err := json.Marshal(pizza)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.Write(jsonPizza)
}

func PizzaCreate(w http.ResponseWriter, r *http.Request) {
	var pizza models.Pizza
	if err := json.NewDecoder(r.Body).Decode(&pizza); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := pizzaEnv.Create(pizza); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func PizzaUpdate(w http.ResponseWriter, r *http.Request) {
	// get id and validate
	id := chi.URLParam(r, "id")

	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if _, err := pizzaEnv.GetById(o_id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// parse and update
	var pizza models.Pizza
	if err := json.NewDecoder(r.Body).Decode(&pizza); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := pizzaEnv.Update(o_id, pizza); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func PizzaDelete(w http.ResponseWriter, r *http.Request) {
	// get id and validate
	id := chi.URLParam(r, "id")

	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if _, err := pizzaEnv.GetById(o_id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := pizzaEnv.Delete(o_id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func PizzaIndexView(w http.ResponseWriter, r *http.Request) {
	// list of files
	var files = []string{
		"pkg/templates/base.html",
		"pkg/templates/pizza/index.html",
	}
	var templates = template.Must(template.ParseFiles(files...))

	pizzas, _ := pizzaEnv.GetAll() // get all pizza records and pass to template

	if err := templates.ExecuteTemplate(w, "base.html", pizzas); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func PizzaCreateView(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		name := r.FormValue("name")
		f_id := r.FormValue("style_id")
		style_id, _ := strconv.Atoi(f_id)
		pizza := models.Pizza{Name: name, StyleID: style_id}
		if err := pizzaEnv.Create(pizza); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/pizza", http.StatusSeeOther)
	}

	// list of files
	var files = []string{
		"pkg/templates/base.html",
		"pkg/templates/form.html",
		"pkg/templates/pizza/create.html",
	}
	var templates = template.Must(template.ParseFiles(files...))

	styles, err := styleEnv.GetAll()
	if err != nil {
		return
	}

	if err := templates.ExecuteTemplate(w, "base.html", styles); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func PizzaUpdateView(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	o_id, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	pizza, err := pizzaEnv.GetById(o_id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	styles, err := styleEnv.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if r.Method == "POST" {
		name := r.FormValue("name")
		f_id := r.FormValue("style_id")
		style_id, _ := strconv.Atoi(f_id)
		pizza := models.Pizza{Name: name, StyleID: style_id}
		if err := pizzaEnv.Update(o_id, pizza); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/pizza", http.StatusSeeOther)
	}

	// list of files
	var files = []string{
		"pkg/templates/base.html",
		"pkg/templates/form.html",
		"pkg/templates/pizza/update.html",
	}
	var templates = template.Must(template.ParseFiles(files...))

	data := map[string]interface{}{
		"pizza":  pizza,
		"styles": styles,
	}

	if err := templates.ExecuteTemplate(w, "base.html", data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
