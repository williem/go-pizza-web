package controllers

import (
	"html/template"
	"net/http"
)

func HomeIndex(w http.ResponseWriter, r *http.Request) {
	files := []string{
		"pkg/templates/base.html",
		"pkg/templates/home/index.html",
	}

	templates := template.Must(template.ParseFiles(files...))
	if err := templates.ExecuteTemplate(w, "base.html", nil); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
